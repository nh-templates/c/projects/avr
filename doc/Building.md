# Building

Building the project is done using the [make file](../Make.sh).

## Steps

The project is generally built in the following steps:
 1. Compilation: compiles the source code into object files
 2. Linking: links the object files and libraries into an **ELF** file
 3. Generation: a **HEX** file is generated from the **ELF** file
 4. Generation: a **SREC** file is generated from the **ELF** file
 5. Generation: an **EEP** file containing EEPROM byte code is generated
 6. Dumping: an **LSS** listings file is dumped
 7. Calculation: the size of the generated output is calculated and printed

## Build Options

Projects can be built in multiple modes.

### Default

By default, projects are built with all project source files in ["/src"](../src)
including the main source file at ["/src/main.c"](../src/main.c).  Debugging is
enabled by defining the `DEBUG` macro at compilation.

```bash
./Make build
```

Building in default mode compiles the source files into individual output files
in ["/build/source"](../build/source) and compiles the main file in
["/build"](../build).  The files are then linked into multiple assembled files
in ["/bin"](../bin) as specified in the file structure detailed in the
[template documentation](./Template.md).

### Testing

In test mode, all project source files in ["/src"](../src) -- exempting the main
file, ["/src/main.c"](../src/main.c) -- are built along with the specified
test's source file.  Debugging is enabled by defining the `DEBUG` macro at
compilation.

```bash
./Make build -t TEST_NAME
./Make build --test TEST_NAME
```

Both options build the project with the "TEST_NAME" test.  The test name
specifies the test source file to include in compilation and linking.  The test
name is the base name of the C source file in ["/test"](../test), so the source
for a test called "*TEST_NAME*" is located at "[/test](../test)/*TEST_NAME.c*".

Building in testing mode compiles the source files into individual output files
in ["/build/source"](../build/source) and compiles the test file in
["/build/test"](../build/test).  The output files are then linked into multiple
assembled files in "[/bin](../bin)/*TEST_NAME*" where "*TEST_NAME*" is the name
of the selected test as specified in the file structure detailed in the
[template documentation](./Template.md).

### Production

In production mode, all project source files in ["/src"](../src) -- including
the main file ["/src/main.c"](../src/main.c) -- are built, and debugging is
disabled by not defining the `DEBUG` macro at compilation.

```bash
./Make build -p
./Make build --production
```

Both options build the project in production mode.  Building in production mode
re-compiles the source files into individual output files in
["/build/source"](../build/source) and compiles the main file in
["/build"](../build). The files are then linked into multiple assembled files in
["/bin"](../bin) as specified in the file structure detailed in the
[template documentation](./Template.md).

### Other Options

There are a few other specific options to control the build process.

#### Keeping Byte Code Sections

During regular linking, some byte code sections are removed by default.
The "*fuse*", "*lock*", and "*user_signature*" sections will be removed from
the assembled files unless otherwise specified.

 - Keeping the "*fuse*" section
   - `--keep-fuse`
 - Keeping the "*lock*" section
   - `--keep-lock`
 - Keeping the "*user_signatures*" section
   - `--keep-user_signatures`

#### Generating an SREC File

If specified, an **SREC** file can be generated along with the **HEX** file.

 - Generate an **SREC** file
   - `--generate-srec`

#### Not Generating EEP File

If specified, an **EEP** file containing EEPROM byte code will not be generated.

 - Not generating an **EEP** file
   - `--no-eep`

#### Not Dumping LSS File

If specified, an **LSS** file containing program listings will not be generated.

 - Not generating an **LSS** file
   - `--no-lss`

#### Not Dumping TXT File

If specified, a **TXT** file containing program details will not be generated.
 - Not generating a **TXT** file
   - `--no-txt`

## Command Line

Below are the commands and options used in each of the build steps.

### Compilation (avr-gcc)
 - Select the micro-controller
   - `-mmcu=MCU_ID`
 - Set the standard library version
   - `-std=C_VERSION`
 - Set the language explicitly
   - `-x c`
 - Compile only
   - `-c`
 - Make all warnings errors
   - `-Werror`
 - Include questionable warnings as errors
   - `-Wall`
 - Print pedantic error messages
   - `-pedantic-errors`
 - Optimize the output for size
   - `-Os`
 - Make char be unsigned unless signed
   - `-funsigned-char`
 - Make bitfields be unsigned
   - `-funsigned-bitfields`
 - Limits inline function size to 3
   - `-finline-limit=3`
 - Place each function item in its own section
   - `-ffunction-sections`
 - Place each data item in its own section
   - `-fdata-sections`
 - Define the DEBUG macro
   - `-DDEBUG`
 - Override default dependency output file
   - `-MF"source.d"`
 - Add phony target for each dependency
   - `-MP`
 - Set the target of dependency generation
   - `-MT"source.d" -MT"source.o"`
 - Generate a dependency file for the output
   - `-MD`
 - Add directories to be searched for headers
   - `-Iinclude -Ilib`
 - Optimize memory use for enums
   - `-fshort-enums`
 - Pack all struct members together without holes
   - `-fpack-struct`
 - Function prologues are expanded for optimization
   - `-mcall-prologues`
 - Output file
   - `-o"source.o"`
 - Input file
   - `"source.c"`

### Linking (avr-ld)
 - Adds "lib/importedlib" to the list of searched library folders
   - `-Llib/importedlib`
 - Prevents dynamic linking
   - `-static`
 - Garbage collect unused input sections
   - `--gc-sections`
 - Generate a map file
   - `-Map="bin/target.map"`
 - Print garbage collected sections
   - `--print-gc-sections`
 - Perform target-specific global optimization
   - `--relax`
 - Start the library listing
   - `--start-group -lm -lc -limportedlib ... --end-group`
 - Output file
   - `-o"bin/target.elf"`
 - Input file list
   - `"source1.o" "source2.o" "source3.o" ...`

### Generating HEX File (avr-objcopy)
 - Select "*ihex*" output format
   - `-O ihex`
 - Remove unused sections
   - `-R .eeprom -R .fuse -R .lock -R .signature -R .user_signatures`
 - Source file
   - `"bin/target.elf"`
 - Output file
   - `"bin/target.hex"`

### Generating SREC File (avr-objcopy)
 - Select "*srec*" output format
   - `-O srec`
 - Remove unused sections
   - `-R .eeprom -R .fuse -R .lock -R .signature -R .user_signatures`
 - Source file
   - `"bin/target.elf"`
 - Output file
   - `"bin/target.hex"`

### Generating EEP File (avr-objcopy)
 - Select "*ihex*" output format
   - `-O ihex`
 - Select only the .eeprom section
   - `-j .eeprom`
 - Set flags for data section
   - `--set-section-flags=.eeprom=alloc,load`
 - Change where section is loaded to memory
   - `--change-section-lma .eeprom=0`
 - Do not warn about lma change
   - `--no-change-warnings`
 - Source file
   - `"bin/target.elf"`
 - Output file
   - `"bin/target.eep"`

### Dumping LSS File (avr-objdump)
 - Display summary information from section headers
   - `-h`
 - Display source code intermixed with disassembly
   - `-S`
 - Source file
   - `"bin/target.elf"`
 - Forward output file
   - `> "bin/target.lss"`

### Calculating Size (avr-size)
 - Source file
   - `"bin/target.elf"`
 - Forward output file
   - `> "bin/target.txt"`

## Related
 - [Deploying](./Deploying.md)
 - [Libraries](./Libraries.md)
