# Deploying

Deploying the project is done using the [make file](../Make.sh).

## Deployment Options

Deployment is done using one of two modes: default or testing.

### Default

By default, projects are deployed using the HEX file at the base of the
["/bin"](../bin) directory.  Building in both debug mode and production mode
produces the file located here, so depending on how the project was last built,
deployment may deploy the production build or the debug build.

```bash
./Make deploy
```

For more details on how the build mode affects the ["/bin"](../bin) directory,
see the [building documentation](./Building.md).

### Testing

In test mode, projects are deployed using the HEX file at the base of the
"[/bin](../bin)*/TEST_NAME*" directory, where "*TEST_NAME*" is the name of the
built test.

```bash
./Make deploy -t TEST_NAME
./Make deploy --test TEST_NAME
```

Both options deploy the testing version of the built code.  For more details on
the build process of tests, see the [building documentation](./Building.md).

### Other Options

There are a few other specific options available to control the deployment
process.

#### Deploying EEPROM Data

Instead of deploying the HEX code for the project, the EEPROM data can be
written alone using the `-e` option.

#### Device Signature Checking

By default, the deployment tool checks the device signature to confirm the
write destination.  This behavior can be disabled using the `--no-check` option.

#### Chip Erase

By default, all chip data is preserved except for that being written.  A full
chip erase can be performed before the write using the `--erase` option.

#### Safe Mode

By default, the deployment tool has "**safe mode**" enabled which checks the
device's fuses for changes during the write.  This behavior can be disabled
using the `--no-safemode` option.

## Command Line

Below are the commands and options used in deployment.

### Deployment (avrdude)

 - Be verbose
   - `-v`
 - Set the programmer name
   - `-c usbasp`
 - Set the port type
   - `-P usb`
 - Set the processor type
   - `-p atmega328p`
 - Override device signature checking
   - `-F`
 - Cause chip erase
   - `-e`
 - Perform a memory operation
   - `-U flash:w:bin/target.hex:i`
   - `-U eeprom:w:bin/target.eep:i`
 - Disable safemode fuse bit checks
   - `-u`

## Related

 - [Building](./Building.md)
