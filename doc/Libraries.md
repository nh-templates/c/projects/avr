# Libraries

Importing libraries is important functionality for this project template.  It
allows for external, reusable code to be imported directly into the project.

## Standardization

The importing of libraries is based off of the file structure of the
[library template](https://gitlab.com/nh-templates/c/libraries/avr).  The major
components, however, boil down to a few points:

### Archive Files

The archive file is the compiled output of the built library.  In order for the
library to be imported correctly, this must be located in a "*bin*" directory
which should be inside the base directory of the project.

### Header Files

The header files are used to import external functionality.  In order for the
library to be imported correctly, these must be located in an "include"
directory which should be in the base directory of the project.  

To include these header files, they can be referenced directly, so any global
header files at the base directory can be included normally.  So, to import a
global header "*header1.h*" in the library called "*LIBRARY_NAME*", it can just be
imported as "*header1.h*".

However, the local library headers are by standard in a directory with the
same name as the library.  So, to import a local header "*header2.h*" in the
library called "*LIBRARY_NAME*", it must be imported as
"*LIBRARY_NAME/header2.h*".

### Auto-Building

In the building of the project, if a library is not built, it is built
recursively by executing its **Make** file.  It is not necessary if the project
is already built, but to enable auto-building, a make file must be present.  The
**Make** file will be executed as:

```bash
./Make build
```

## Related
 - [Template](./Template.md)
