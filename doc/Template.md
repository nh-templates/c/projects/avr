# The Template

This template is meant to serve as a base for AVR-C projects.  It defines an
encompassing file structure, build tools, git resources, and pre-written
documentation for the project.

## Using The Template

In order to use this template to create a project, it must be cloned to a local
directory.  This can be accomplished by navigating to the appropriate directory,
cloning the repository, and completing the setup.

### Cloning

The repository can be cloned from [GitLab](http://gitlab.com) by executing the
following command:

```bash
git clone git@gitlab.com:nh-templates/c/projects/avr.git PROJECT_NAME
```

where "*PROJECT_NAME*" is the desired name of the project.

### Setup

Before the project can be utilized, a few minor changes must be made to the
project in order to customize it to the desired project.

#### Git Changes

The project's "*origin*" remote must be changed (or removed) so the project
does not track the template's repository.  This can be accomplished by
executing:

```bash
git remote rename origin template
```

or

```bash
git remote remove origin
```

to not track any remote repositories.

**Note:** The remote "*template*" can be used to update the project template
after cloning.

#### Make File

The Make file is completely plug-and-play, but by default it is configured for
the Atmega328P and C99.  To change these defaults, the "[/Make.sh](../Make.sh)"
file must be edited.

##### Changing the Chip Code

To change the chip code, the "**PROJECT_AVR**" bash variable must be changed to
match one of the chip codes specified in the **AVRDUDE**
[manual](https://www.nongnu.org/avrdude/user-manual/avrdude_4.html#Option-Descriptions).

##### Changing the C Version

To change the C version, the "**PROJECT_CVER**" bash variable must be changed to
match one of the C versions accepted by the **AVR-GCC** manual.

#### Git Keep Files

In order for certain empty folders to be tracked by Git, it was necessary to add
useless "*.gitkeep*" files at the base of these directories.  Directories
containing a "*.gitkeep*" file are:

 - "[/bin](../bin)"
 - "[/build](../build)"
 - "[/include](../include)"
 - "[/lib](../lib)"
 - "[/test](../test)"

These "*.gitkeep*" files are useless and can be deleted manually or by running
a command.

```bash
# You must first navigate to the project base directory
rm ./bin/.gitkeep ./build/.gitkeep ./include/.gitkeep ./lib/.gitkeep ./test/.gitkeep
```

## File Structure

The project's file structure separates source code, compiler output, third
party libraries, tests, documentation, and project files into separate
paradigms.  The general file structure is described below.

### File Tree
- **program1** *- The base project folder*
  - **bin** *- Assembled bytecode and output*
    - **test1** *- Assembled bytecode and output from the "test1" test*
      - test1.elf
      - test1.hex
      - test1.srec
      - test1.eep
      - test1.lss
      - test1.txt
    - program1.elf *- Executable and linkable format file*
    - program1.hex *- Intel ASCII byte code file*
    - program1.srec *- Optional Motorola S-Record file*
    - program1.eep *- Raw EEPROM byte code dump*
    - program1.lss *- Decompiled listings file*
    - program1.txt *- Section and size data*
  - **build** *- Compiled object files*
    - **source** *- Compiled object files from source files*
      - source1.o
      - source2.o
      - source3.o
    - **test** *- Compiled object files from test files*
      - test1.o
      - test2.o
      - test3.o
    - main.o *- The compiled object file from the main source file*
  - **doc** *- Project documentation*
    - Building.md
    - Deploying.md
    - Document1.md
    - Document2.md
    - Document3.md
    - Libraries.md
    - Template.md
  - **include** *- C header files*
    - header1.h
    - header2.h
    - header3.h
  - **lib** *- 3rd Party libraries, see "[Libraries](./Libraries.md)"*
    - external-library1
      - ... *- Please reference documentation in the AVR-C library project*
  - **src** *- C source files*
    - source1.c
    - source2.c
    - source3.c
  - **test** *- C unit and system tests*
    - test1.c
    - test2.c
    - test3.c
  - .gitignore *- Git configuration, specifies files to not track*
  - .gitmodules *- Git configuration, specifies project sub-modules*
  - LICENSE *- A GNU general public license*
  - Make.sh *- The project build script, see "Build Tools" section*
  - README.md *- The project README file*


## Build Tools

This project is built, tested, and deployed using the Make file.  It compiles
source files from "[/src](../src)" and "[/test](../test)" and compiles them
into "[/build](../build)" according to the source type.  The compiled output
files are then linked together and with libraries in "[/lib](../lib)" and the
output is sent to "[/bin](../bin)".  The resulting bytecode is then deployed
to the AVR device.

To build the program, you must execute the bash script in **build mode**:
```bash
# Build the project in debug mode with default options
./Make build

# Build the project in test mode with test "TEST_NAME"
./Make build -t TEST_NAME
# or
./Make build --test TEST_NAME

# Build the project in production mode
./Make build -p
# or
./Make build --production
```

To deploy the program, you must execute the bash script in **deploy mode**:
```bash
# Deploy the debug/production program with default options
./Make deploy

# Deploy the test called "TEST_NAME"
./Make deploy -t TEST_NAME
# or
./Make deploy --test TEST_NAME
```

To clean the program, you must execute the bash script in **cleaning mode**:
```bash
# Clean the "bin" and "build" directories
./Make clean
```

For further information, read the bash script's help:
```bash
./Make help
```
or reference the respective documentation:
 - [Building](./Building.md)
 - [Deploying](./Deploying.md)
 - [Libraries](./Libraries.md)

## Git Resources

When this project is cloned, it is created with a fully initialized Git
instance.  It also includes a couple extra configuration files.

### Git Ignore

The git ignore file specifies files and directories that are to be left
untracked.

Official documentation can be found at
[https://git-scm.com/docs/gitignore](https://git-scm.com/docs/gitignore).

### Git Modules

The git modules file defines all git submodules within the scope of the current
git instance.

Official documentation can be found at
[https://git-scm.com/docs/gitmodules](https://git-scm.com/docs/gitmodules)

## Documentation

This project is automatically populated with a set of necessary documentation.

### Template Documentation

All template functionality is already documented (this file being one instance).
This includes documentation for the Make file such as for
[building](./Building.md), [deploying](./Deploying.md), and
[libraries](./Libraries.md).

### Licensing

The template is made with a
[GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html)
created by the [Free Software Foundation](http://fsf.org/).  It short, it
specifies that the software is free to run, study, share, or modify.  All
derivative work can also only be distributed under the same license terms.  This
helps keep software and digital resources free for the public, as all things
should be.

### README

The template is also made with a default README file.  This file is meant to be
blown away once the template is completely set up.  It is often used as the
"documentation landing page," so it is meant to contain project-specific
documentation.
