# AVR-C Project Template V1.1

This is a generic, standardized template for AVR-C projects.

## Usage

1. To use this template for your project, you must first set up a repository for
your new project.

2. Then, you must first clone
[this template](https://gitlab.com/nh-templates/c/projects/avr).  Navigate to
the directory you want to create your new project in, and clone this project
with a new name.

```bash
git clone git@gitlab.com:nh-templates/c/projects/avr.git PROJECTNAME
```

3. Next, you must replace the remote.

```bash
git remote rename origin template
git remote add origin REPOSITORYURL
```

4. Now, the project is fully functional, and an initial commit is optional.

```bash
git commit -a -m "Initial commit"
git push -u origin --all
```

**Note:** for further help, see the [template documentation](./doc/Template.md)

## Features

This project template comes with multiple convenient features:
- Standard project file system
- Pre-written documentation
- Git configuration files
- GNU General Public License
- README
- Make file
